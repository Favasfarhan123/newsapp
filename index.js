const blogContainer=document.getElementById('blog-container')



const apiKey= "b89addfd3cd2474ca2d022207a59884c"

const url="https://newsapi.org/v2/top-headlines?country=us"

async function fetchNews(){
    try{
        const response=await fetch(`https://newsapi.org/v2/top-headlines?country=in&apiKey=${apiKey}`)
        const data= await response.json()
        console.log(data)
       return data.articles
       
        }

    catch (error) {
        console.error(error);
    
}
}




function displayBlogs(news){
    
    blogContainer.innerHTML=""
   if(news){
    news.forEach(articles => {
        const blogCard=document.createElement("div")
        //blogCard.classList.add("blog-card")
        const img=document.createElement("img")
        img.src=articles.urlToImage
        img.alt=articles.title
        const title=document.createElement("h2")
        title.textContent=articles.title
        const description=document.createElement("p")
        description.textContent=articles.description
        blogCard.classList.add("blog-card")    
        const button=document.createElement("a")
        button.href=articles.url
        button.innerText="Read More"
        
        
        blogCard.appendChild(img)
        blogCard.appendChild(title)
        blogCard.appendChild(description)
        blogContainer.appendChild(blogCard)
        blogContainer.classList.add("container")
        blogContainer.classList.add("blog-container")
        blogCard.appendChild(button)  
        if(!articles.urlToImage){
            img.src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"

        }
    } )}}
        



(async ()=>{
    try{
         const news=await fetchNews()
         displayBlogs(news)

    }
    catch(error) {
        console.log(error)
    }
})
();